import {Component, ViewChild} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import { Chart } from 'chart.js';
/**
 * Generated class for the BankPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bank',
  templateUrl: 'bank.html',
})
export class BankPage {
  @ViewChild('barCanvas') barCanvas;
  barChart: any;
  data:any;
  constructor(public modalctrl:ModalController,public navCtrl: NavController, public navParams: NavParams,public viewCtrl : ViewController) {

  }
  public closeModal(){
    this.viewCtrl.dismiss();
  }
  ionViewDidLoad() {
    this.data=this.navParams.get("message");
    console.log(this.data);
    let labe=Object.keys(this.data.configuration);let vals=[];
    let taille=labe.length;let i=0;

    for(i=0;i<taille;i++){
        vals.push(this.data.configuration[labe[i]]);
    }
    let appreciation="appréciation:"+this.data.appreciation;
    console.log(labe);
    this.barChart = new Chart(this.barCanvas.nativeElement, {

      type: 'doughnut',
      data: {
        labels: labe,
        datasets: [{
          label: '# of Votes',
          data: vals,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        }
      }

    });
    // this.barChart = new Chart(this.barCanvas.nativeElement, {
    //   type: 'doughnut',
    //   data: {
    //     labels: labe,
    //     datasets: [{
    //       label:[appreciation],
    //       data:vals,
    //       backgroundColor: [
    //         'rgba(255, 99, 132, 0.2)',
    //         'rgba(54, 162, 235, 0.2)',
    //         'rgba(255, 206, 86, 0.2)',
    //         'rgba(75, 192, 192, 0.2)',
    //         'rgba(153, 102, 255, 0.2)',
    //         'rgba(255, 159, 64, 0.2)'
    //       ],
    //       borderColor: [
    //         'rgba(255,99,132,1)',
    //         'rgba(54, 162, 235, 1)',
    //         'rgba(255, 206, 86, 1)',
    //         'rgba(75, 192, 192, 1)',
    //         'rgba(153, 102, 255, 1)',
    //         'rgba(255, 159, 64, 1)'
    //       ],
    //       borderWidth: 1
    //     }]
    //   },
    //   options: {
    //     scales: {
    //       yAxes: [{
    //         ticks: {
    //           beginAtZero:true
    //         }
    //       }]
    //     }
    //   }
    //
    // });
  }


}
