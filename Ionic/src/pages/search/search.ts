import { Component } from '@angular/core';
import {IonicPage, LoadingController, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import { Item } from '../../models/item';
import { Items } from '../../providers';
import {BanlProvider} from "../../providers/banl/banl";
import {LoginPage} from "../login/login";
import {EncaissementPage} from "../encaissement/encaissement";

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
  currentItems: any;
  token:any;
  InitValues(val:boolean){
    let loading = this.Loadingctrl.create({
      spinner:'hide',
      cssClass:'Loadings',
      content: '<img src="assets/img/Block2.svg">',
      showBackdrop: false
    });
    if(val) {
      loading.present();
    }
    this.native.get("token").then((data)=>{
      if(data==null) {
        this.navCtrl.setRoot(LoginPage);
      }
      else{
        this.token=data;
      }
    }).then((data)=>{
      console.log("OK");
      this.bp.MyAccounts(this.token).then((val:any)=>{
        console.log(val);
        if(val.error == 1){
          this.native.remove("token");
          this.navCtrl.setRoot(LoginPage);
        }
        else{
          this.currentItems=val.data;
          this.native.set("listecomptes",val.data);
        }
        if(val) {
          loading.dismiss();
        }
      }).catch(error=>{
        this.native.get("listecomptes").then((val)=>{
          if(val==null){
            this.createToast("Connexion Perdue");
          }
          else{
            this.currentItems=val;
          }
          if(val) {
            loading.dismiss();
          }
        });
      });
    })
  }
  constructor(public toastCtrl:ToastController,public native:Storage,public Loadingctrl:LoadingController,public bp:BanlProvider,public navCtrl: NavController, public navParams: NavParams, public items: Items, public modalCtrl: ModalController) {
    this.InitValues(true);

  }
  createToast(message){
    let toast = this.toastCtrl.create({
      message:message,
      duration:10,
      position: 'top'
    });
    toast.present();
  }
  /**
   * Perform a service for the proper items.
   */
  getItems(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.currentItems = this.items.query({
      name: val
    });
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(banque:any) {
    console.log(banque);
    const data = { message : banque };
    const modalpage = this.modalCtrl.create('BankPage',data);
    modalpage.present();
  }
  Encaisser(clientbanque:any) {
    console.log(clientbanque);
    const data = { message : clientbanque };
    const modalpage = this.modalCtrl.create('EncaissementPage',data);
    modalpage.present();
  }
  Decaisser(clientbanque:any) {
    console.log(clientbanque);
    const data = { message : clientbanque };
    const modalpage = this.modalCtrl.create('DecaissementPage',data);
    modalpage.present();
  }

}
