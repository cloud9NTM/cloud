import { Component } from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import {MvtProvider} from "../../providers/mvt/mvt";
import {LoginPage} from "../login/login";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the DecaissementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-decaissement',
  templateUrl: 'decaissement.html',
})
export class DecaissementPage {
  montant:number=0;
  numero:string;
  obk:any;
  message:string;
  token:any;
  constructor(public toastctr:ToastController,public alert:AlertController,public native:Storage,public mvt:MvtProvider,public modalctr:ModalController,public navCtrl: NavController, public navParams: NavParams) {
    this.native.get("token").then((data)=>{
      if(data){
        this.token=data;
      }
      else{
        this.navCtrl.setRoot(LoginPage);
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EncaissementPage');
    this.obk=this.navParams.get("message");
    console.log(this.obk);
  }

  createToast(title,subtitle){
    const alert = this.alert.create({
      title:title,
      subTitle:subtitle,
      buttons: ['OK']
    });
    alert.present();
  }
  envoi(){
    this.mvt.envoi(this.numero,this.montant,this.token,this.obk.b.id).then((data:any)=>{
      if(data.error == 1){
        this.createToast("erreur",data.message);
      }
      else{
        this.createToast("Success","");
      }
    }).catch((error)=>{
      this.createToast("erreur",error);
    });
  }
}
