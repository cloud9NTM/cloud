import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DecaissementPage } from './decaissement';

@NgModule({
  declarations: [
    DecaissementPage,
  ],
  imports: [
    IonicPageModule.forChild(DecaissementPage),
  ],
})
export class DecaissementPageModule {}
