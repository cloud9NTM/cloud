import { Component } from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import {MvtProvider} from "../../providers/mvt/mvt";
import {LoginPage} from "../login/login";
import {Storage} from "@ionic/storage";
/**
 * Generated class for the EncaissementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-encaissement',
  templateUrl: 'encaissement.html',
})
export class EncaissementPage {
  montant:number=0;
  devise:string;
  obk:any;
  message:string;
  token:any;
  constructor(public toastctr:ToastController,public alert:AlertController,public native:Storage,public mvt:MvtProvider,public modalctr:ModalController,public navCtrl: NavController, public navParams: NavParams) {
      this.native.get("token").then((data)=>{
        if(data){
          this.token=data;
        }
        else{
          this.navCtrl.setRoot(LoginPage);
        }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EncaissementPage');
    this.obk=this.navParams.get("message");
    console.log(this.obk);
  }

  createToast(title,subtitle){
    const alert = this.alert.create({
      title:title,
      subTitle:subtitle,
      buttons: ['OK']
    });
    alert.present();
  }

  Encaissement(){
    this.mvt. EncaisserClient(this.devise,this.montant,this.token,this.obk.b.id).then((data:any)=>{
      if(data.error ==1){
        this.createToast("erreur",data.message);
      }
      else{
        this.createToast("Sucsess","");
      }
    }).catch((error)=>{
      this.createToast("erreur",error);
    });
  }
}
