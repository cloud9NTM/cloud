import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items } from '../../providers';
import {BanlProvider} from "../../providers/banl/banl";
import {Storage} from "@ionic/storage";
import {LoginPage} from "../login/login";
import {ToastController} from "ionic-angular";
import {LoadingController} from "ionic-angular";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/interval';
@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  currentItems:any;
  token:any=null;

  /**
   * The view loaded, let's query our items for the list
   */
  InitValues(val:boolean){
    let loading = this.Loadingctrl.create({
      spinner:'hide',
      cssClass:'Loadings',
      content: '<img src="assets/img/Block2.svg">',
      showBackdrop: false
    });
    if(val) {
      loading.present();
    }
    this.native.get("token").then((data)=>{
      if(data==null) {
        this.navCtrl.setRoot(LoginPage);
      }
      else{
        this.token=data;
      }
    }).then((data)=>{
      console.log("OK");
      this.bp.Bank(this.token).then((val:any)=>{
        console.log(val);
        if(val.error == 1 ){
          this.native.remove("token");
          this.navCtrl.setRoot(LoginPage);
        }
        else{
          this.currentItems=val.data;
          this.native.set("listebanque",val.data);
        }
        if(val) {
          loading.dismiss();
        }

      }).catch(error=>{
        this.native.get("listebanque").then((val)=>{
          if(val==null){
            this.createToast("Connexion Perdue");
          }
          else{
            this.currentItems=val;
          }
          if(val) {
            loading.dismiss();
          }
        });
      });
    })
  }
  constructor(public Loadingctrl:LoadingController,public toastCtrl:ToastController,public native:Storage,public bp:BanlProvider,public navCtrl: NavController, public items: Items, public modalCtrl: ModalController) {
    this.InitValues(true);
    Observable.interval(20000).subscribe((val)=>{
        this.InitValues(false);
    });
  }

  ionViewDidLoad() {
  }
  createToast(message){
    let toast = this.toastCtrl.create({
      message:message,
      duration:10,
      position: 'top'
    });
    toast.present();
  }
  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    let addModal = this.modalCtrl.create('ItemCreatePage');
    addModal.onDidDismiss(item => {
      if (item) {
        this.items.add(item);
      }
    });
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.items.delete(item);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(banque:any) {
    console.log(banque);
    const data = { message : banque };
    const modalpage = this.modalCtrl.create('BankPage',data);
    modalpage.present();
  }
}
