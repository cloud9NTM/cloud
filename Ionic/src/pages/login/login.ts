import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import {LogingogoProvider} from "../../providers/logingogo/logingogo";
import { User } from '../../providers';
import { MainPage } from '../';
import {Storage} from "@ionic/storage";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email:'',
    password: ''
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(private nativeStorage: Storage,public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,public lps:LogingogoProvider) {
    this.nativeStorage.get("token").then((value)=>{
      console.log("valeur="+value);
        if(value!=null){
          this.navCtrl.setRoot(MainPage);
        }
    });
  }
  doLogin() {
    this.lps.Login(this.account.email,this.account.password).then((data:any)=>{
    if(data.error == 1){
      console.log("ato");
      let toast = this.toastCtrl.create({
        message:data.message,
        duration: 1,
        position: 'top'
      });
      toast.present();
    }
    else{
      console.log(data);
      this.nativeStorage.set("token",data.data.chain);
      this.navCtrl.setRoot(MainPage);
    }
    }).catch(error=>{
      let toast = this.toastCtrl.create({
        message: error,
        duration: 1,
        position: 'top'
      });
      toast.present();
    });

  }
  // Attempt to login in through our User service
}
