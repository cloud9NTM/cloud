import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { User } from '../../providers';
import {} from '../../providers';
import { MainPage } from '../';
import {LogingogoProvider} from "../../providers/logingogo/logingogo";

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { name: string, phoneNumber: string, password: string } = {
    name: 'UserName',
    phoneNumber: '0347151720',
    password: 'test'
  };

  // Our translated text strings
  private signupErrorString: string=null;

  constructor(public navCtrl: NavController,
    public user: User,public lps:LogingogoProvider,
    public toastCtrl: ToastController,
    public translateService: TranslateService) {
    this.signupErrorString = null;
  }

  doSignup() {
    // Attempt to login in through our User service
    console.log(this.account.phoneNumber);
    console.log(this.account.password);
    this.lps.Inscription(this.account.name,this.account.phoneNumber,this.account.password).then((data:any)=> {
      if (data!=null && !data.isUndefined) {
        this.signupErrorString = data.message;
        if(data.error == 0){
          console.log("OK");
        }
      }
    }).catch(error=>{console.log(error)})
  }
}
