package Services;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.web.client.RestTemplate;

public class DeviseService{

    public static double getTaux(String unite,String unitecible) throws Exception{
        RestTemplate rt=new RestTemplate();
        try{
            Gson g=new Gson();
            String val=unite+"_"+unitecible;
            System.out.println(val);
            String taux =rt.getForObject("https://free.currencyconverterapi.com/api/v6/convert?q="+val+"&compact=ultra&apiKey=3403d429f4bca4bfc28b",String.class);
            String[] ls = taux.substring(1,taux.length()-1).split(":");
            return Double.valueOf(ls[1]);
        }
        catch(Exception e){
            e.printStackTrace();
            throw e;
        }
    }
}