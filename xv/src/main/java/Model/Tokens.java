package Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Tokens {
    @Id
    private String id;
    private String chain;
    private String idutil;

    public Tokens(String id, String chain, String idutil, Date datedebut, Date datefin, Integer status) {
        this.id = id;
        this.chain = chain;
        this.idutil = idutil;
        this.datedebut = datedebut;
        this.datefin = datefin;
        this.status = status;
    }

    public String getIdutil() {

        return idutil;
    }

    public void setIdutil(String idutil) {
        this.idutil = idutil;
    }

    private Date datedebut;
    private Date datefin;
    private Integer status;
    public Tokens(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChain() {
        return chain;
    }

    public void setChain(String chain) {
        this.chain = chain;
    }

    public Date getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(Date datedebut) {
        this.datedebut = datedebut;
    }

    public Date getDatefin() {
        return datefin;
    }

    //    public long getDatefin(){
//        long time=this.datefin.getTime()-this.getDatedebut().getTime();
//        return time;
//    }
    public void setDatefin(Date datefin) {
        this.datefin = datefin;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
