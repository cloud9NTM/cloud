/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import Services.DeviseService;

/**
 *
 * @author FATF
 */
public class Monnaie {
    private double valeur;
    private String unit;

    public Monnaie(double valeur, String unit) {
        this.setValeur(valeur);
        this.setUnit(unit);
    }
    public Monnaie(String valeur, String unit) {
        this.setValeur(valeur);
        this.setUnit(unit);
    }

    public Monnaie() {

    }

    public double getValeur() {
        return valeur;
    }
    public void setValeur(String valeur) {
        this.valeur = Double.valueOf(valeur);
    }

    public void setValeur(double valeur) {
        this.valeur = valeur;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    public Monnaie Convert(String unite) throws Exception{
        if(this.unit.equals(unite)){
            return this;
        }
        return new Monnaie(DeviseService.getTaux(this.unit, unite)*this.valeur,unite);
    }
}
