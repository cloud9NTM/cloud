package Model;

public class Etat {
    private String id;
    private double totalDebit;
    private double totalCredit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(double totalDebit) {
        this.totalDebit = totalDebit;
    }

    public double getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(double totalCredit) {
        this.totalCredit = totalCredit;
    }
    public Etat(){}

    public Etat(String id, double totalDebit, double totalCredit) {
        this.id = id;
        this.totalDebit = totalDebit;
        this.totalCredit = totalCredit;
    }
}
