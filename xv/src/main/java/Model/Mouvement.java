package Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.HashMap;
@Document
public class Mouvement {
    @Id
    private String id;
    private String type;
    private String idsource;
    private String nomsource;

    public String getNomsource() {
        return nomsource;
    }

    public void setNomsource(String nomsource) {
        this.nomsource = nomsource;
    }

    private ClientBanque cb;
    private HashMap<String, Double> Configurations;
    private HashMap<String, Double> MappedConfigurations;
    private String deviseOrigine;
    private int confirmed;
    private double montantFrais;
    private double debit;
    private double credit;
    private Date date;

    public double getDebit() {
        return debit;
    }

    public void setDebit(double debit) {
        this.debit = debit;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdsource() {
        return idsource;
    }

    public void setIdsource(String idsource) {
        this.idsource = idsource;
    }

    public ClientBanque getCb() {
        return cb;
    }

    public void setCb(ClientBanque cb) {
        this.cb = cb;
    }

    public HashMap<String, Double> getConfigurations() {
        return Configurations;
    }

    public void setConfigurations(HashMap<String, Double> configurations) {
        Configurations = configurations;
    }

    public HashMap<String, Double> getMappedConfigurations() {
        return MappedConfigurations;
    }

    public void setMappedConfigurations(HashMap<String, Double> mappedConfigurations) {
        MappedConfigurations = mappedConfigurations;
    }

    public String getDeviseOrigine() {
        return deviseOrigine;
    }

    public void setDeviseOrigine(String deviseOrigine) {
        this.deviseOrigine = deviseOrigine;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }


    public double getTotalFrais() {
        return montantFrais;
    }

    public void setMontantFraisontant(double montantFrais) {
        this.montantFrais = montantFrais;
    }

    public double getMontantFrais() {
        return montantFrais;
    }

    public void setMontantFrais(double montantFrais) {
        this.montantFrais = montantFrais;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Mouvement(){}
    public Mouvement(String type, String idsource, ClientBanque cb, HashMap<String, Double> configurations, HashMap<String, Double> mappedConfigurations, String deviseOrigine, int confirmed, double montantFrais, double debit, double credit,Date date) {
        this.type = type;
        this.idsource = idsource;
        this.cb = cb;
        Configurations = configurations;
        MappedConfigurations = mappedConfigurations;
        this.deviseOrigine = deviseOrigine;
        this.confirmed = confirmed;
        this.montantFrais = montantFrais;
        this.debit = debit;
        this.credit = credit;
        this.date = date;
    }
}
