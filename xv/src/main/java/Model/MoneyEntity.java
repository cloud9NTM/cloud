/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author FATF
 */
public class MoneyEntity {
    private Monnaie m;
    private double pourcentage;
    private String nomconfig;

    public String getNomconfig() {
        return nomconfig;
    }

    public void setNomconfig(String nomconfig) {
        this.nomconfig = nomconfig;
    }

    public MoneyEntity(String valeur, String devise, double pourcentage,String nomconfig) {
        Monnaie m=new Monnaie();m.setUnit(devise);m.setValeur(valeur);
        m.setValeur((m.getValeur()*pourcentage)/100);
        this.setM(m);
        setNomconfig(nomconfig);
    }
    public MoneyEntity(Monnaie m, double pourcentage) {
        this.setM(m);
        this.setPourcentage(pourcentage);
    }

    public Monnaie getM() {
        return m;
    }

    public void setM(Monnaie m) {
        this.m = m;
    }

    public double getPourcentage() {
        return pourcentage;
    }

    public void setPourcentage(double pourcentage) {
        this.pourcentage = pourcentage;
    }
    
}
