package Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document
public class ClientBanque {
    @Id
    private String id;
    private String idClient;
    private String numeroCompte;
    private Banque b;
    private double solde;
    private String devise;
    private int estCloture;
    private String phoneNumber;


    public boolean equals(ClientBanque o) {
        if(o!=null){
           if(o.getId()==this.getId()){
                return true;
           }
        }
        return false;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public String getNumeroCompte() {
        return numeroCompte;
    }

    public void setNumeroCompte(String numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ClientBanque(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Banque getB() {
        return b;
    }

    public void setB(Banque b) {
        this.b = b;
    }
    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public int getEstCloture() {
        return estCloture;
    }

    public void setEstCloture(int estCloture) {
        this.estCloture = estCloture;
    }

    public String getIdClient() { return idClient; }

    public void setIdClient(String idClient) { this.idClient = idClient; }

    public double getSolde() { return solde; }

    public void setSolde(double solde) { this.solde = solde; }
}
