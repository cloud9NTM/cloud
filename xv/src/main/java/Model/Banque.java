package Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@Document
public class Banque {
    @Id
    private String id;
    private String nom;
    private long positionx;
    private long positiony;
    private String devise;
    private double compte;
    private String appreciation="";

    private static  transient String favorableHS="Favorable a l'echange inter-banque";
    private static  transient String favorableCL="Favorable aux echanges internes";
    private static  transient String neutral="Neutre";


    public String getAppreciation() {
        double cl=Configuration.get("TauxCLHS");
        double hs=Configuration.get("TauxCLCL");
        if(hs>cl){
            return favorableHS;
        }
        else if(hs<cl){
            return favorableCL;
        }
        else{
            return neutral;
        }
    }

    public void setAppreciation(String appreciation) {
        if(appreciation != null) {
            this.appreciation = appreciation;
        }
        else{
            this.appreciation = getAppreciation();
        }
    }
    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public double getCompte() {
        return compte;
    }
    public Banque(){}
    public void setCompte(double compte) {
        this.compte = compte;
    }

    private HashMap<String,Double> Configuration=new HashMap();

    public HashMap<String, Double> getConfiguration() {
        return Configuration;
    }

    public void setConfiguration(HashMap<String, Double> configuration) {
        Configuration = configuration;
    }

    public Banque(String id, String nom, long positionx, long positiony) {
        this.id = id;
        this.nom = nom;
        this.positionx = positionx;
        this.positiony = positiony;
    }
    public void setPosition(String positionx,String positiony) throws Exception{
        this.positionx=Long.valueOf(positionx);
        this.positiony=Long.valueOf(positiony);
    }
    public void setListe(HashMap<String,Object>listex){
        try {
            Configuration.put("TauxFixe", Double.valueOf(listex.get("TauxFixe").toString()));
        }
        catch(Exception e){
            Configuration.put("TauxFixe",0.0);
        }
        try {
            Configuration.put("TauxCLCL", Double.valueOf(listex.get("TauxCLCL").toString()));
        }
        catch(Exception e){
            Configuration.put("TauxCLCL",0.0);
        }
        try {
            Configuration.put("TauxCLHS", Double.valueOf(listex.get("TauxCLHS").toString()));
        }
        catch(Exception e){

            Configuration.put("TauxCLHS",0.0);
        }
    };
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getPositionx() {
        return positionx;
    }

    public void setPositionx(long positionx) {
        this.positionx = positionx;
    }

    public long getPositiony() {
        return positiony;
    }

    public void setPositiony(long positiony) {
        this.positiony = positiony;
    }
}
