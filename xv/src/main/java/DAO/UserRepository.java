package DAO;

import Model.Login;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
public interface UserRepository extends MongoRepository<Login, String> {

}