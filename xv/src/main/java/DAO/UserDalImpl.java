package DAO;

import Model.Banque;
import Model.ClientBanque;
import Model.Login;
import Model.Tokens;
import Security.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
@Repository
public class UserDalImpl implements UserDAL{
    public static final int estadmin=1;public static final int notadmin=0;
    public static final int estcloture=0;
    public static final int notcloture=1;
    @Autowired
    private MongoTemplate mt;

    @Autowired
    private SequenceDao sequenceDao;

    public MongoTemplate getMt() {
        return mt;
    }

    public void setMt(MongoTemplate mt) {
        this.mt = mt;
    }

    @Override
    @Transactional
    public String inscription(String username,String phonenumber,String mdp,int estadmin) throws Exception{
        try {System.out.println("vloilasd"+username);
            Query q=new Query(Criteria.where("nom").is(username));
            Login l=mt.findOne(q,Login.class);
            if(l!=null){
                throw new Exception("Ce nom d'utilisateur est déjà pris");
            }
            l=new Login();
            l.setPhoneNumber(phonenumber);
            l.setNom(username);
            l.setPassword(Hash.hash(mdp));
            l.setIsAdmin(estadmin);
            mt.save(l);
            return "Success";
        }
        catch(Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public Login auth(String tokenString,int type) throws Exception {
        Date d=new Date();
        Query q = new Query(Criteria.where("chain").is(tokenString).and("datefin").gt(d));
        Tokens t=mt.findOne(q,Tokens.class);
        Login lx=null;
        if(t!=null) {
            lx= mt.findById(t.getIdutil(), Login.class);
            System.out.println(lx.getIsAdmin());
            System.out.println("type");
        }
        else{
            throw new Exception("Invalid Token");
        }
        if(lx==null || lx.getIsAdmin()!=type){
            throw new Exception("Invalid Token");
        }
        return lx;
    }

    @Override
    @Transactional
    public String creationClientBanque(Login l, String idbanque,String devise) throws Exception {
        String numero = "BK"+sequenceDao.getNextSequenceId("numeroCompte");
        ClientBanque cb=new ClientBanque();
        cb.setB(mt.findById(idbanque, Banque.class));
        cb.setSolde(0);
        cb.setNumeroCompte(numero);
        cb.setIdClient(l.getId());
        cb.setDevise(devise);
        cb.setEstCloture(0);
        cb.setPhoneNumber(l.getPhoneNumber());
        mt.save(cb);
        return numero;
    }


    private static final long decalage=60*3*1000;
    @Override
    @Transactional
    public Tokens testLogin(String nom, String mdp,int estadmin) throws Exception{
        try {
            String password = Security.Hash.hash(mdp);
            Query q = new Query(Criteria.where("nom").is(nom).and("password").is(password).and("isAdmin").is(estadmin));
            Login l = mt.findOne(q, Login.class);
            Tokens t = new Tokens();
            if (l != null) {
                t.setChain(Security.Hash.hash(l.getId() + l.getNom() + new Timestamp(System.currentTimeMillis()).toString()));
                t.setIdutil(l.getId());
                Date datedebut=new Date();
                datedebut.setTime(datedebut.getTime()+decalage);
                t.setDatedebut(datedebut);
                Date d = new Date();
                System.out.println(d.toString());
                d.setTime(d.getTime() + 72000000+decalage);
                t.setDatefin(d);
                t.setStatus(UserDAL.working);
                mt.save(t);
            } else {
                throw new Exception("Cannot find Login");
            }
            return t;
        }
        catch(Exception e){
            throw e;
        }
    }
}
