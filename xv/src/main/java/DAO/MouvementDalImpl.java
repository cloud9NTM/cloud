package DAO;

import Model.*;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Repository
public class MouvementDalImpl implements MouvementDAL {

    @Autowired
    private MongoTemplate mt;
    @Override
    @Transactional
    public void encaisser(String idClient,String idBanque,String devise,double montant) throws Exception{
        System.out.println("idC :"+idClient);
        System.out.println("idB :"+idBanque);
        if(montant<=0){
            throw new Exception("Montant invalide");
        }
        if(!devise.equalsIgnoreCase("EUR")&&!devise.equalsIgnoreCase("MGA")&&!devise.equalsIgnoreCase("USD")){
            throw new Exception("Devise invalide");
        }
        ObjectId idB = new ObjectId(idBanque);
        Query q=new Query(Criteria.where("idClient").is(idClient).and("b._id").is(idB));
        ClientBanque cb = mt.findOne(q,ClientBanque.class);
        if(cb!=null){
            Monnaie money = new Monnaie(montant,devise);
            Monnaie convMoney = money.Convert(cb.getDevise());
            Mouvement m = new Mouvement(MouvementDAL.encaissement,null,cb,null,null,devise,0,0,0,convMoney.getValeur(),new Date());
            mt.save(m);
            //cb.setSolde(cb.getSolde()+montant);
            //mt.save(cb);
        }
        else{
            throw new Exception("Compte inexistant");
        }
    }
    @Override
    @Transactional
    public void decaisser(Login l1, String idBanque1, String numero2,String valeur) throws Exception{
        BanqueDalImpl bdi=new BanqueDalImpl();
        bdi.setMt(mt);
        UserDalImpl udi=new UserDalImpl();
        udi.setMt(mt);
        ClientBanque cl=bdi.getCompte(l1.getId(),idBanque1);
        double valeurfixe=Double.valueOf(valeur);
        if(valeurfixe<=0){
            throw new Exception("Montant invalide");
        }
        String type=MouvementDAL.decaissementCLCL;
        Query q = new Query(Criteria.where("numeroCompte").is(numero2));
        ClientBanque client2=mt.findOne(q,ClientBanque.class);
        if(client2!=null) {
            HashMap<String, Double> listeconfigs = client2.getB().getConfiguration();
            Monnaie m = new Monnaie(valeur, cl.getDevise());
            MoneyEntity FraisFixe = new MoneyEntity(valeur, cl.getDevise(), listeconfigs.get("TauxFixe"), "TauxFixe");
            MoneyEntity fraisadd = null;
            if (cl.getB().getId().equals(client2.getB().getId())) {
                fraisadd = new MoneyEntity(valeur, cl.getDevise(), listeconfigs.get("TauxCLCL"), "TauxCLCL");
            } else {
                type = MouvementDAL.decaissementCLHS;
                fraisadd = new MoneyEntity(valeur, cl.getDevise(), listeconfigs.get("TauxCLHS"), "TauxCLHS");
            }
            HashMap<String, Double> mappedvalues = new HashMap();
            Monnaie fraistotaux = new Monnaie(FraisFixe.getM().getValeur() + fraisadd.getM().getValeur(), m.getUnit());
            //montant + frais (Miala @ le mandefa)
            Monnaie totalmontant = new Monnaie(valeurfixe + fraistotaux.getValeur(), cl.getDevise());
            if(totalmontant.getValeur()>cl.getSolde()){
                throw new Exception("Monnaie Insuffisante");
            }
            mappedvalues.put(FraisFixe.getNomconfig(), FraisFixe.getM().getValeur());
            mappedvalues.put(fraisadd.getNomconfig(), fraisadd.getM().getValeur());
            //mahazo
            Mouvement mx = new Mouvement(type, cl.getId(), client2, null, null, cl.getDevise(), 0, fraistotaux.Convert(client2.getDevise()).getValeur(), 0, m.Convert(client2.getDevise()).getValeur(), new Date());
            mx.setNomsource(l1.getNom());
            mt.save(mx);
            //mandefa
            Mouvement m1 = new Mouvement(type, null, cl, listeconfigs, mappedvalues, cl.getDevise(), 1, fraistotaux.getValeur(), totalmontant.getValeur(), 0, new Date());
            mt.save(m1);
           // System.out.println("solde: "+cl.getSolde()+" montant:"+totalmontant.getValeur());
            cl.setSolde(cl.getSolde()-totalmontant.getValeur());
            mt.save(cl);
            client2.setSolde(client2.getSolde()+m.Convert(client2.getDevise()).getValeur());
            mt.save(client2);
            Banque b1 = cl.getB();
            //System.out.println("id :"+b1.getId()+" "+fraistotaux.getValeur());
            b1.setCompte(b1.getCompte()+fraistotaux.getValeur());
            mt.save(b1);
            ObjectId idB = new ObjectId(b1.getId());
            Query query = Query.query(Criteria.where("cb.b._id").is(idB));
            Update updt = new Update().set("cb.b", b1);
            mt.updateMulti(query, updt, Mouvement.class);
            Query query2 = Query.query(Criteria.where("b._id").is(idB));
            updt = new Update().set("b", b1);
            mt.updateMulti(query2, updt, ClientBanque.class);

        }
        else{
            throw new Exception("Numero de compte inconnue");
        }
    }


    @Override
    @Transactional
    public double getSolde(String idClient,String idBanque,Date debut,Date fin) throws Exception{
        MatchOperation match = Aggregation.match(new Criteria().andOperator(Criteria.where("idClient").is(idClient),Criteria.where("b.id").is(idBanque),Criteria.where("date").gt(debut),Criteria.where("date").lt(fin),Criteria.where("confirmed").is(0)));
        GroupOperation group = Aggregation.group("cb.id").sum("debit").as("totalDebit").sum("credit").as("totalCredit");
        ProjectionOperation project = Aggregation.project("cb.id","totalDebit","totalCredit");
        Aggregation aggr = Aggregation.newAggregation(match,group,project);
        AggregationResults<Etat> result = mt.aggregate(aggr,"mouvement",Etat.class);
        Etat etat = result.getUniqueMappedResult();
        if(etat!=null){
            return etat.getTotalCredit()-etat.getTotalDebit();
        }
        else{
            throw new Exception("Compte inexistant");
        }
    }

    @Override
    public List<Mouvement> getListeMouvementsNA() throws Exception {
        Query q=new Query(Criteria.where("type").is("0").and("confirmed").is(0));
        return mt.find(q,Mouvement.class);
    }
    @Override
    public List<Mouvement> getListeMouvement(String idClient){
        Query q=new Query(Criteria.where("cb.idClient").is(idClient));
        q.with(new Sort(new Sort.Order(Sort.Direction.DESC, "date")));
        q.skip(0);
        q.limit(10);
        return mt.find(q,Mouvement.class);
    }

    @Override
    public void confirmerEncaissement(String idMouvement) throws Exception{
        Mouvement m = mt.findById(idMouvement,Mouvement.class);
        if(m == null){
            throw new Exception("Mouvement Invalide");
        }
        m.setConfirmed(1);
        mt.save(m);
        ClientBanque cb = mt.findById(m.getCb().getId(),ClientBanque.class);
        cb.setSolde(cb.getSolde()+m.getCredit());
        mt.save(cb);
    }

}
