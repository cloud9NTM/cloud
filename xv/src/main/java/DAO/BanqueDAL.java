package DAO;

import Model.Banque;
import Model.ClientBanque;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface BanqueDAL {

    public List<Banque> getListear(int type);
    public Banque findBanquebyId(String id);
    public void DeleteBanque(String id);
    public void modifier(String idB,String nom,HashMap<String,Object> configs,String devise) throws Exception ;
    public void CreateBanque(Banque b);
    public List<Banque> getListeBanqueClient(String idClient);
    public List<ClientBanque> getAllClientAccount(String idClient) throws Exception;
    public ClientBanque getCompte(String idClient,String idBanque) throws Exception;
    public Banque getBanque(String idBanque) throws Exception;
}
