package DAO;

import Model.Login;
import Model.Mouvement;

import java.util.Date;
import java.util.List;

public interface MouvementDAL {
    public static final String encaissement="0";
    public static final String decaissementCLCL="1";
    public static final String decaissementCLHS="2";
    public void decaisser(Login l1, String idBanque1, String numero2, String valeur) throws Exception;
    public void encaisser(String idClient, String idBanque,String devise,double montant) throws Exception;
    public double getSolde(String idClient, String idBanque, Date debut, Date fin) throws Exception;
    public List<Mouvement> getListeMouvement(String idClient);
    public List<Mouvement> getListeMouvementsNA() throws Exception;
    public void confirmerEncaissement(String idMouvement) throws Exception;
}
;