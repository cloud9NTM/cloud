package DAO;

import Model.Banque;
import Model.ClientBanque;
import Model.Login;
import Model.Mouvement;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Repository
public class BanqueDalImpl implements BanqueDAL {
    @Autowired
    private MongoTemplate mt;

    public MongoTemplate getMt() {
        return mt;
    }

    public void setMt(MongoTemplate mt) {
        this.mt = mt;
    }

    @Override
    public List<Banque> getListear(int i)
    {
        Query q=new Query();
        if(i==UserDAL.notAdmin){
            q.fields().exclude("compte");
        }
        return mt.find(q,Banque.class);
    }

    @Override
    public Banque findBanquebyId(String id) {
        return mt.findById(id,Banque.class);
    }

    @Override
    @Transactional
    public void DeleteBanque(String id) {
        Query q=Query.query(Criteria.where("idbanque").is(id));
        mt.findAllAndRemove(q, Mouvement.class);
        mt.findAllAndRemove(q,ClientBanque.class);
        q=Query.query(Criteria.where("id").is(id));
        mt.findAndRemove(q,Banque.class);
    }

    @Override
    @Transactional
    public void modifier(String idB,String nom,HashMap<String,Object> configs,String devise) throws Exception {
        Banque b = mt.findById(idB,Banque.class);
        if(b!=null) {
            b.setNom(nom);
            b.setListe(configs);
            b.setDevise(devise);
            ObjectId id = new ObjectId(b.getId());
            Query query = Query.query(Criteria.where("cb.b._id").is(id));
            Update updt = new Update().set("cb.b", b);
            mt.updateMulti(query, updt, Mouvement.class);
            Query query2 = Query.query(Criteria.where("b._id").is(id));
            updt = new Update().set("b", b);
            mt.updateMulti(query2, updt, ClientBanque.class);
            mt.save(b);
        }
        else{
            throw new Exception("Banque inexistant");
        }
    }
    @Override
    @Transactional
    public void CreateBanque(Banque b){
        mt.save(b);
    }

    @Override
    @Transactional
    public List<Banque> getListeBanqueClient(String idClient){
        Query q=Query.query(Criteria.where("idClient").is(idClient));
        List<ClientBanque> result = mt.find(q,ClientBanque.class);
        ArrayList<Banque> listeBanque = new ArrayList();
        for(ClientBanque cb : result){
            listeBanque.add(cb.getB());
        }
        System.out.println("size : "+listeBanque.size());
        return listeBanque;
    }

    @Override
    @Transactional
    public ClientBanque getCompte(String idClient,String idBanque) throws Exception {
        ObjectId idB = new ObjectId(idBanque);
        Query q=new Query(Criteria.where("idClient").is(idClient).and("b._id").is(idB));
        ClientBanque cb = mt.findOne(q,ClientBanque.class);
        if(cb == null){
            throw new Exception("Ce client n'a pas de compte dans cette banque");
        }
        return cb;
    }

    @Override
    @Transactional
    public List<ClientBanque> getAllClientAccount(String idClient) throws Exception{
        Query q=new Query(Criteria.where("idClient").is(idClient));
        List<ClientBanque> listCb = mt.find(q,ClientBanque.class);
        if(listCb!=null){
            return listCb;
        }
        else{
            throw new Exception("Compte inexistant");
        }
    }

    @Override
    @Transactional
    public Banque getBanque(String idBanque) throws Exception{
        return mt.findById(idBanque,Banque.class);
    }
}
