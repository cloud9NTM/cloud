package DAO;

import Model.Login;
import Model.Tokens;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface UserDAL {
	public static final int expired=400;
	public static final int working=200;
	public static final int estAdmin=1;
	public static final int notAdmin=0;
	public Tokens testLogin(String nom, String mdp,int estadmin) throws Exception;
	public String inscription(String username,String phonenumber,String mdp,int estadmin) throws Exception;
    public Login auth(String tokenString,int type) throws Exception;
    public String creationClientBanque(Login l,String idbanque,String devise) throws Exception;
}