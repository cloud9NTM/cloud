package DAO;

public interface SequenceDao {

	long getNextSequenceId(String key) throws Exception;

}