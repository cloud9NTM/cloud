package Controllers;

import DAO.BanqueDAL;
import DAO.MouvementDAL;
import DAO.UserDAL;
import DAO.UserRepository;
import Model.Banque;
import Model.Login;
import Model.Mouvement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("/BanqueAdmin")
public class BanqueAdminController {
    @Autowired
    private MouvementDAL md ;
    @Autowired
    private UserDAL ur;
    @Autowired
    private BanqueDAL bd;
    @Autowired
    private UserRepository ure;
    @PostMapping("/Creation")
    public ResponseEntity<HashMap> CreationBanque(@RequestBody Map<String, Object> params){
        HashMap<String,Object>liste=new HashMap();
        try{
            System.out.println(params.toString());
            HashMap<String,Object>listepar=(HashMap)params.get("params");
            Login l=ur.auth(params.get("Token").toString(),UserDAL.estAdmin);
            Banque b=new Banque();
            b.setCompte(0);b.setListe(listepar);
            b.setDevise(params.get("devise").toString());
            b.setNom(params.get("nom").toString());
            b.setPositionx(0);b.setPositiony(0);
            bd.CreateBanque(b);
            liste.put("status",200);
            liste.put("error",0);
            liste.put("message","Success");
            liste.put("data",null);
        }
        catch(Exception e){
            e.printStackTrace();
            liste.put("status",404);
            liste.put("error",1);
            liste.put("message",e.getMessage());
            liste.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(liste, HttpStatus.OK);
        }
    }
    @PostMapping("/ConfigurerBanque")
    public ResponseEntity<HashMap>Configuration(@RequestBody Map<String,Object>params){
        HashMap<String,Object>liste=new HashMap();
        try{
            ur.auth(params.get("Token").toString(),UserDAL.estAdmin);
        }
        catch(Exception e){
            liste.put("status",404);
            liste.put("error",1);
            liste.put("message",e.getMessage());
            liste.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(liste, HttpStatus.OK);
        }
    }
    @PostMapping("/ListeBanque")
    public ResponseEntity<HashMap> getListeBanque(@RequestBody Map<String,Object>params){
        HashMap<String,Object>liste=new HashMap();
        try{
            ur.auth(params.get("Token").toString(),UserDAL.estAdmin);
            //Login l=ur.auth(token,1);
            List<Banque> listeBanque = bd.getListear(UserDAL.estAdmin);
            liste.put("status",200);
            liste.put("error",0);
            liste.put("message",null);
            liste.put("data",listeBanque);
        }
        catch(Exception e){
            e.printStackTrace();
            liste.put("status",404);
            liste.put("error",1);
            liste.put("message",e.getMessage());
            liste.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(liste, HttpStatus.OK);
        }
    }

    @PostMapping("/ListeMouvementNC")
    public ResponseEntity<HashMap> getListeMouvementNC(@RequestBody Map<String, Object> params){
        HashMap<String,Object>data=new HashMap();
        try{
            ur.auth(params.get("Token").toString(),UserDAL.estAdmin);
            List<Mouvement> liste = md.getListeMouvementsNA();
            System.out.println("size :"+liste.size());
            data.put("status",200);
            data.put("error",0);
            data.put("message",null);
            data.put("data",liste);
        }
        catch(Exception e){
            e.printStackTrace();
            data.put("status",404);
            data.put("error",1);
            data.put("message",e.getMessage());
            data.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(data,HttpStatus.OK);
        }
    }
    @PostMapping("/Confirmer")
    public ResponseEntity<HashMap> confirmerEncaissement(@RequestBody Map<String, Object> params){
        HashMap<String,Object>data=new HashMap();
        try{
            ur.auth(params.get("Token").toString(),UserDAL.estAdmin);
            md.confirmerEncaissement(params.get("idM").toString());
            data.put("status",200);
            data.put("error",0);
            data.put("message","Encaissement Confirmé !");
            data.put("data",null);
        }
        catch(Exception e){
            e.printStackTrace();
            data.put("status",404);
            data.put("error",1);
            data.put("message",e.getMessage());
            data.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(data,HttpStatus.OK);
        }
    }
    @PostMapping("/Modification")
    public ResponseEntity<HashMap> modifierBanque(@RequestBody Map<String, Object> params){
        HashMap<String,Object>liste=new HashMap();
        try{
            System.out.println(params.toString());
            HashMap<String,Object>listepar=(HashMap)params.get("params");
            Login l=ur.auth(params.get("Token").toString(),UserDAL.estAdmin);
            bd.modifier(params.get("idB").toString(),params.get("nom").toString(),listepar,params.get("devise").toString());
            liste.put("status",200);
            liste.put("error",0);
            liste.put("message","Modification effectué");
            liste.put("data",null);
        }
        catch(Exception e){
            e.printStackTrace();
            liste.put("status",404);
            liste.put("error",1);
            liste.put("message",e.getMessage());
            liste.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(liste, HttpStatus.OK);
        }
    }

    @PostMapping("/Banque")
    public ResponseEntity<HashMap> getBanque(@RequestBody Map<String, Object> params){
        HashMap<String,Object>liste=new HashMap();
        try{
            System.out.println(params.get("Token").toString());
            Login l=ur.auth(params.get("Token").toString(),UserDAL.estAdmin);
            System.out.println("idB :"+params.get("idB").toString());
            Banque b = bd.getBanque(params.get("idB").toString());
            liste.put("status",200);
            liste.put("error",0);
            liste.put("data",b);
            liste.put("message",null);
        }
        catch(Exception e){
            e.printStackTrace();
            liste.put("status",404);
            liste.put("error",1);
            liste.put("message",e.getMessage());
            liste.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(liste, HttpStatus.OK);
        }
    }
}
