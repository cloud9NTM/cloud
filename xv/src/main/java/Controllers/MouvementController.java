package Controllers;

import DAO.MouvementDAL;
import DAO.UserDAL;
import Model.Banque;
import Model.Login;
import Model.Mouvement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("/Mouvement")
public class MouvementController {
    @Autowired
    private MouvementDAL md ;
    @Autowired
    private UserDAL ur;

    @PostMapping("/Encaisser")
    public ResponseEntity<HashMap> encaisser(@RequestBody Map<String, Object> params){
        HashMap<String,Object>data=new HashMap();
        try{
            Login l=ur.auth(params.get("Token").toString(),UserDAL.notAdmin);
            md.encaisser(l.getId(),params.get("idBanque").toString(),params.get("devise").toString(),Double.valueOf(params.get("montant").toString()));
            data.put("status",200);
            data.put("error",0);
            data.put("message","Encaissement Effectué !");
            data.put("data",null);
        }
        catch(Exception e){
            e.printStackTrace();
            data.put("status",404);
            data.put("error",1);
            data.put("message",e.getMessage());
            data.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(data,HttpStatus.OK);
        }
    }
    @PostMapping("/Decaisser")
    public ResponseEntity<HashMap> decaisser(@RequestBody Map<String, Object> params){
        HashMap<String,Object>data=new HashMap();
        try{
            Login l=ur.auth(params.get("Token").toString(),UserDAL.notAdmin);
            md.decaisser(l,params.get("idBanque").toString(),params.get("numero").toString(),params.get("montant").toString());
            data.put("status",200);
            data.put("error",0);
            data.put("message","Decaissement Effectué !");
            data.put("data",null);
        }
        catch(Exception e){
            e.printStackTrace();
            data.put("status",404);
            data.put("error",1);
            data.put("message",e.getMessage());
            data.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(data,HttpStatus.OK);
        }
    }
    @PostMapping("/ListeMouvement")
    public ResponseEntity<HashMap> getListeMouvement(@RequestBody Map<String, Object> params){
        HashMap<String,Object>data=new HashMap();
        try{
            Login l=ur.auth(params.get("Token").toString(),UserDAL.notAdmin);
            List<Mouvement> liste = md.getListeMouvement(l.getId());
            data.put("status",200);
            data.put("error",0);
            data.put("message",null);
            data.put("data",liste);
        }
        catch(Exception e){
            e.printStackTrace();
            data.put("status",404);
            data.put("error",1);
            data.put("message",e.getMessage());
            data.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(data,HttpStatus.OK);
        }
    }
}
