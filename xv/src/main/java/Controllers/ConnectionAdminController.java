package Controllers;
import DAO.BanqueDAL;
import DAO.UserDAL;
import DAO.UserRepository;
import Model.Login;
import Model.Tokens;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/ConnectionAdmin")
public class ConnectionAdminController {
    @Autowired
    private UserDAL ur;
    @Autowired
    private UserRepository ure;
    @Autowired
    private BanqueDAL bd;

    @PostMapping("/Inscription")
    public ResponseEntity<HashMap>Inscription(@RequestBody Map<String, Object> params){
        String nom = (String)params.get("nom");
        String password = (String)params.get("password");
        String number=(String)params.get("numero");
        System.out.println(nom+" "+password);
        HashMap<String,Object>data=new HashMap();
        try{
            ur.inscription(nom,number,password,1);
            data.put("status",200);
            data.put("error",0);
            data.put("message","Success");
            data.put("data",null);
        }
        catch(Exception e){
            data.put("status",404);
            data.put("error",1);
            data.put("message",e.getMessage());
            data.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(data,HttpStatus.OK) ;
        }
    }

    @PostMapping("/Connexion")
    public ResponseEntity<HashMap>connexion(@RequestBody Map<String, Object> params){
        String nom = (String)params.get("nom");
        String password = (String)params.get("password");
        HashMap<String,Object>data=new HashMap();
        try{
            Tokens t = ur.testLogin(nom,password,1);
            data.put("status",200);
            data.put("error",0);
            data.put("message",null);
            data.put("data",t);
        }
        catch(Exception e){
            data.put("status",404);
            data.put("error",1);
            data.put("message",e.getMessage());
            data.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(data,HttpStatus.OK) ;
        }
    }
}
