package Controllers;

import DAO.BanqueDAL;
import DAO.UserDAL;
import DAO.UserRepository;
import Model.Banque;
import Model.ClientBanque;
import Model.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("/Banque")
public class BanqueController {
    @Autowired
    private UserDAL ur;
    @Autowired
    private BanqueDAL bd;
    @Autowired
    private UserRepository ure;
    private List<Banque> listear=null;

    private List<Banque> getListear() {
        return bd.getListear(UserDAL.notAdmin);
    }
    private Banque getFromListe(String id){
        List<Banque>l=getListear();
        int taille=l.size();Banque b=null;
        for(int i=0;i<taille;i++){
            b=l.get(i);
            if(b.getId().equals(id)){
                return b;
            }
        }
        return null;
    }

    public void setListear(List<Banque>listear) {
        this.listear = listear;
    }
    @PostMapping("/ListeBanqueClient")
    public ResponseEntity<HashMap> getListeBanqueCLient(@RequestBody Map<String,Object>params){
        HashMap<String,Object>liste=new HashMap();
        try{
            Login l=ur.auth(params.get("Token").toString(),UserDAL.notAdmin);
            List<Banque> listeBanque = bd.getListeBanqueClient(l.getId());
            liste.put("status",200);
            liste.put("error",0);
            liste.put("message",null);
            liste.put("data",listeBanque);
        }
        catch(Exception e){
            e.printStackTrace();
            liste.put("status",404);
            liste.put("error",1);
            liste.put("message",e.getMessage());
            liste.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(liste, HttpStatus.OK);
        }
    }
    @PostMapping("/ListeBanque")
    public ResponseEntity<HashMap> getListeBanque(@RequestBody Map<String,Object>params){
        HashMap<String,Object>liste=new HashMap();
        try{
            Login l=ur.auth(params.get("Token").toString(),UserDAL.notAdmin);
            List<Banque> listeBanque = getListear();
            liste.put("status",200);
            liste.put("error",0);
            liste.put("message",null);
            liste.put("data",listeBanque);
        }
        catch(Exception e){
            e.printStackTrace();
            liste.put("status",404);
            liste.put("error",1);
            liste.put("message",e.getMessage());
            liste.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(liste, HttpStatus.OK);
        }
    }

    @PostMapping("/AllAccount")
    public ResponseEntity<HashMap> getAllAccount(@RequestBody Map<String,Object>params){
        HashMap<String,Object>data=new HashMap();
        try {
            Login log = ur.auth(params.get("Token").toString(), UserDAL.notAdmin);
            List<ClientBanque> listCb = bd.getAllClientAccount(log.getId());
            data.put("status",200);
            data.put("error",0);
            data.put("message",null);
            data.put("data",listCb);
        }
        catch(Exception e){
            e.printStackTrace();
            data.put("status",404);
            data.put("error",1);
            data.put("message",e.getMessage());
            data.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(data,HttpStatus.OK) ;
        }
    }
}
