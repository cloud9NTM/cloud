package Controllers;

import DAO.BanqueDAL;
import DAO.UserDAL;
import DAO.UserRepository;
import Model.ClientBanque;
import Model.Login;
import Model.Tokens;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/Connection")
public class ConnectionController {
    @Autowired
    private UserDAL ur;
    @Autowired
    private UserRepository ure;


    @PostMapping("/Inscription")
    public ResponseEntity<HashMap>Inscription(@RequestBody Map<String, Object> params){
        String nom = (String)params.get("nom");
        String password = (String)params.get("password");
        String number=(String)params.get("numero");
        System.out.println(nom+" "+password);
        HashMap<String,Object>data=new HashMap();
        try{
            ur.inscription(nom,number,password,0);
            data.put("status",200);
            data.put("error",0);
            data.put("message","Success");
            data.put("data",null);
        }
        catch(Exception e){
            data.put("status",404);
            data.put("error",1);
            data.put("message",e.getMessage());
            data.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(data,HttpStatus.OK) ;
        }
    }

    @PostMapping("/Connexion")
    public ResponseEntity<HashMap>connexion(@RequestBody Map<String, Object> params){
        String nom = (String)params.get("nom");
        String password = (String)params.get("password");
        HashMap<String,Object>data=new HashMap();
        try{
            Tokens t = ur.testLogin(nom,password,0);
            data.put("status",200);
            data.put("error",0);
            data.put("message",null);
            data.put("data",t);
        }
        catch(Exception e){
            data.put("status",404);
            data.put("error",1);
            data.put("message",e.getMessage());
            data.put("data",null);
        }
        finally{
            return new ResponseEntity<HashMap>(data,HttpStatus.OK) ;
        }
    }
    @PostMapping("/CreateAccount")
    public ResponseEntity<HashMap>createAccount(@RequestBody Map<String, Object> params) {
        String idBanque = (String) params.get("idBanque");
        String devise = (String) params.get("devise");

        HashMap<String, Object> data = new HashMap();
        try {
            Login l = ur.auth(params.get("Token").toString(), 0);
            String numero = ur.creationClientBanque(l, idBanque, devise);
            data.put("status",200);
            data.put("error",0);
            data.put("message", "Ouverture Reussie ! Votre numero de compte est " + numero);
            data.put("data",null);
        } catch (Exception e) {
            e.printStackTrace();
            data.put("status",404);
            data.put("error",1);
            data.put("message", "Vous avez déja un compte dans cette banque");
            data.put("data",null);
        } finally {
            return new ResponseEntity<HashMap>(data, HttpStatus.OK);
        }
    }
}
